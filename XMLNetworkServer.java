import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * XMLNetworkServer bere XML in ga potem nazaj poslje odjemalcu 
 * Server moramo ustaviti s CTRL+C (streze v neskoncnost)
 * 
 * predelano iz: Core
 * 
 Server se izvala na mali distribuirani napravici...
 */

public class XMLNetworkServer {
	private int port;

	public XMLNetworkServer(int port) {
		this.port=port;
	}

	/**
	 * Monitor a port for connections. Each time one is established, pass
	 * resulting Socket to handleConnection.
	 */

	public void listen() throws SAXException {
		try {
			ServerSocket listener = new ServerSocket(port);
			Socket server;

			System.out.println("Zaganjam streznik....");
			while (true) {
				server = listener.accept();
				handleConnection(server);
			}

		} catch (IOException ioe) {
			System.out.println("IOException: " + ioe);
			ioe.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * V tej metodi je definirano, kaj streznik dela s sporocili odjemalca.
	 */

	protected void handleConnection(Socket server) throws IOException, SAXException {
		//odpri ustrezne vhodne in izhodne tokove za readUTF in writeUTF
		DataOutputStream out = new DataOutputStream(server.getOutputStream());
		DataInputStream in = new DataInputStream(server.getInputStream());

		//preberi XML, ki ga je poslal odjemalec
		String input = in.readUTF();

		// razcleni XML
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		Document doc = null;
		try {
			doc = factory.newDocumentBuilder().parse(new InputSource(new StringReader(input)));
		} catch (SAXException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			System.exit(1);
		}

		// poisci ime osebe
		String serverName = doc.getElementsByTagName("name").item(0).getTextContent();
		String serverDesc = doc.getElementsByTagName("description").item(0).getTextContent();
		String returnFile = doc.getElementsByTagName("returnFile").item(0).getTextContent();
		System.out.println("Communcation with server: " + serverName);
		
		// ustvari XML sporocilo - za demo bo dovolj kar enakega kot prej
		String output = null;
		try {
			output = serializeDocumentToString(buildResponseDocument(returnFile));
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			System.exit(1);
		}

		//poslji sporocilo in zapri povezavo
		out.writeUTF(output);
		
		//beri vrstico (samo da se ustavi program - da ahko poka�emo povezavo z netstat)
		//BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
	    //String s = bufferRead.readLine();
		
	    server.close();
	}

	/**
	 * Create XML document (odgovor streznika odjemalcu)
	 * 
	 * @return Document
	 * @throws ParserConfigurationException
	 */
	private static Document buildResponseDocument(String returnFile) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
		DocumentBuilder parser = fact.newDocumentBuilder();

		File xmlFile = null;
		Document doc = null;
		
		try{
			xmlFile = new File(returnFile);
			doc = parser.parse(xmlFile);
		}catch(Exception e){
			xmlFile = new File("message1.xml");
			doc = parser.parse(xmlFile);
			System.out.println("File does not exist... Returning file message1.xml");
		}
		
		return doc;
	}

	/**
	 * convert Document to String
	 * http://www.theserverside.com/discussions/thread.tss?thread_id=26060
	 */

	private String serializeDocumentToString(Document doc) {
		try {
			DOMSource domSource = new DOMSource(doc);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
			return writer.toString();
		} catch (TransformerException ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public static void main(String[] args) throws SAXException {
	    int port = 1234;
	    if (args.length > 0) {
	      port = Integer.parseInt(args[0]);
	    }
	    XMLNetworkServer nwServer = new XMLNetworkServer(port);
	    nwServer.listen();
	  }
	
}