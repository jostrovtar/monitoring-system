import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.Socket;
import java.net.UnknownHostException;
 
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
 
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
 
import java.io.OutputStream;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
 
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
 
/** 
 */
 
public class XMLNetworkClient {
    protected String host;
    protected int port;
    protected String returnFileName;
    protected long totalTime = 0;
     
 
    /** Register host and port. The connection won't
     *  actually be established until you call
     *  connect.
     */
 
    public XMLNetworkClient(String host, int port, String filename) {
        this.host = host;
        this.port = port;
        this.returnFileName = filename;
    }
 
    /** Establishes the connection, then passes the socket
     *  to handleConnection.
     */
 
    public void connect() {
        try {
            int iter = 100;
             
			writeToLog( this.host);
			 
            for(int i = 0; i < iter; i++){
                Socket client = new Socket(this.host, this.port);
                handleConnection(client);
            }
			
			double avgTime = (double) totalTime / iter;
			
            System.out.println("Total time: " + totalTime);
            System.out.println("Average time: " + avgTime);
			writeToLog(avgTime);
             
        } catch (UnknownHostException uhe) {
            System.out.println("Unknown host: " + host);
            uhe.printStackTrace();
        } catch (IOException ioe) {
            System.out.println("IOException: " + ioe);
            ioe.printStackTrace();
        }
    }
 
    public void writeToLog(long responseTime, String fileName) throws IOException{
        //create if not exists
 
        Writer logFile = new BufferedWriter(new OutputStreamWriter(
        new FileOutputStream("log.txt", true), "utf-8"));
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
         
         
        logFile.write(df.format(Calendar.getInstance().getTime()) + " --- Server host: " + this.host + " --- " + "Transfered file: " + fileName + " --- Response/transfer time: " + responseTime + " ms\n");   
        logFile.close();    
         
    }
	
	public void writeToLog(double avgResponseTime) throws IOException{
		//create if not exists

		Writer logFile = new BufferedWriter(new OutputStreamWriter(
        new FileOutputStream("log.txt", true), "utf-8"));
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
        logFile.write(df.format(Calendar.getInstance().getTime()) + " -- average response time: " + avgResponseTime + "\n------------------------------------------------\n");   
		logFile.close();	
		
	}
	
	public void writeToLog(String host) throws IOException{
		//create if not exists

		Writer logFile = new BufferedWriter(new OutputStreamWriter(
        new FileOutputStream("log.txt", true), "utf-8"));
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
        logFile.write(df.format(Calendar.getInstance().getTime()) + " -- host: " + host + "\n");   
		logFile.close();	
		
	}
     
    /** 
     * Kaj dela odjemalec s sporocili streznika
     */
 
    protected void handleConnection(Socket client) throws IOException {
        DataOutputStream out = new DataOutputStream(client.getOutputStream());
        DataInputStream in = new DataInputStream(client.getInputStream());
 
        //kreiraj DOM Document
        Document doc = null;
        try {
            doc = buildXMLRequest();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            System.exit(1);
        }
 
        //serializiraj Document kot tekst
        String content = serializeDocumentToString(doc);
 
        //measure time
        long startTime = System.currentTimeMillis();
         
        out.writeUTF(content);
        System.out.println("Odgovor streznika: ");
         
        String input = in.readUTF();
         
        //write response time to log file (timestamp --- client ip --- response time)
        long responseTime = System.currentTimeMillis() - startTime;
        writeToLog(responseTime, this.returnFileName);
        totalTime += responseTime;
     
        System.out.println("Response time:" + responseTime + "ms"); 
         
        // razcleni XML
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
         
        try {
            doc = factory.newDocumentBuilder().parse(new InputSource(new StringReader(input)));
        } catch (SAXException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            System.exit(1);
        }
 
        // sparsaj message
        //String deviceID = doc.getElementsByTagName("device_id").item(0).getTextContent();
        //System.out.println(deviceID);
         
        String filename = doc.getElementsByTagName("filename").item(0).getTextContent();
        System.out.println(filename);
         
        //String description = doc.getElementsByTagName("description").item(0).getTextContent();
        //System.out.println(description);
         
        //String sample1 = doc.getElementsByTagName("sample").item(0).getTextContent();
        //System.out.println(sample1);
         
        //String sample2 = doc.getElementsByTagName("sample").item(1).getTextContent();
        //System.out.println(sample2);
        System.out.println("");
         
        client.close();
    }
 
    /** 
     * convert Document to String
     * http://www.theserverside.com/discussions/thread.tss?thread_id=26060
     */
 
    private String serializeDocumentToString(Document doc) {
        try {
            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, result);
            return writer.toString();
        } catch (TransformerException ex) {
            ex.printStackTrace();
            return null;
        }
    }
 
    /**
     * Create XML document
     * @return Document
     * @throws ParserConfigurationException
     */
    private Document buildXMLRequest() throws ParserConfigurationException {
        DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = fact.newDocumentBuilder();
        Document doc = parser.newDocument();
 
        Node server = doc.createElement("server");
        doc.appendChild(server);
 
        Node name = doc.createElement("name");
        server.appendChild(name);
 
        Node description = doc.createElement("description");
        server.appendChild(description);
         
        Node returnFile = doc.createElement("returnFile");
        server.appendChild(returnFile);
 
        name.appendChild(doc.createTextNode("Server-One"));
        description.appendChild(doc.createTextNode("Server descriprion..."));
        returnFile.appendChild(doc.createTextNode(this.returnFileName)); // zaenkrat sta datoteki message1.xml in message2.xml
         
 
        return doc;
    }
     
    public static void main(String[] args) {
        String host = "localhost"; //85.10.24.38
        int port = 1234;   
        String returnFileName = "message1.xml";
         
        if (args.length > 0) {
          host = args[0];
        }
         
        if (args.length > 1) {
          returnFileName = args[1];
        }
         
        XMLNetworkClient nwClient = new XMLNetworkClient(host, port, returnFileName);
        nwClient.connect();
      }
 
}